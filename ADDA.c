#include <avr/io.h>
#include <stdint.h>
#include <util/delay.h>

#include "Codegeruest_ADDA\os_input.h"

// Convenience defines for boolean TRUE and FALSE
#define TRUE 1
#define FALSE 0


typedef enum {
	MANUAL_DA,
	TRACKING_AD,
	SUCCESSIVE_AD
} ProgramType;

const ProgramType programType = TRACKING_AD;

// "Forward-Declarations":
// Prototypes for functions to be implemented
void manuell(void);
void tracking(void);
void sar(void);

// Helper functions
void initDAConverter(void);
void initADConverter(void);
void waitForButtonPress(void);
uint8_t readComparator(void);
void setReferenceVoltage(uint8_t);
uint8_t referenceVoltageInBounds(uint16_t);

int main(void)
{
	switch (programType) {
		case MANUAL_DA: 
			manuell();
			break;
		
		case SUCCESSIVE_AD:
			sar();
			break;

		case TRACKING_AD:
			tracking();
			break;
	};
}

/*!
 * Reads input on PortD and displays value on PortA & PortB
 */
void manuell(void) 
{
	initDAConverter();	
	while(TRUE) {
		// read value of PORTD
		uint8_t readValue = PIND;

		// write value to PORTA and PORTB
		PORTA = readValue;
		PORTB = readValue;
	}
}

/*!
 * Implements a tracking-converter according to given inputs
 */
void tracking(void) 
{
	initADConverter();
	os_initInput();

	while(TRUE) {
		waitForButtonPress();

		uint16_t ref = 127;
		uint8_t direction = 0;

		setReferenceVoltage(ref);
		uint8_t comparatorValue = readComparator();
		direction = comparatorValue;
		
		// tracking once means that we stop when we have gone far enough, so that
		// comparatorValue != direction
		// we also need to stop when we reach the max. Values of 8 bit, 2^8
		while(comparatorValue == direction && referenceVoltageInBounds(ref)) {

			if(comparatorValue == 0) { 
				// u_ref < u_measured
				ref++;
			} else {
				// u_ref > u_measured
				ref--;
			}

			setReferenceVoltage(ref);
			comparatorValue = readComparator();
		}
	}
}

/*!
 * Implements a SAR according to given inputs
 */
void sar(void) {
	initADConverter();
	os_initInput();

	while(TRUE) {
		waitForButtonPress();
		
		uint8_t steps = 8;
		uint8_t ref = 0;
		
		for(int i = steps - 1; i >= 0; i--) {
			uint8_t bitMask = (1 << i);
			// try to set the bit
			ref |= bitMask;
			setReferenceVoltage(ref);
		
			if(readComparator() == 1) {
				// if we have gone too high, delete the bit again
				ref &= ~bitMask;
			}
		}

		setReferenceVoltage(ref);
	}
}

/*!
 * Initializes Ports according to DA-Converter
 */
void initDAConverter(void)
{
	// set port D as input
	DDRD &= 0x00;
	PORTD |= 0xFF;

	// set port A (LEDs) as output
	DDRA |= 0xFF;

	// set port B (resistors) as output
	DDRB |= 0xFF;
}

/*!
 * Initializes Ports according to AD-Converter
 */
void initADConverter(void)
{
	// Initialize pin C0 (comparator) as input
	DDRC &= ~(1 << 0);
//	PORTC |= (1 << 0);

	// set port A (LEDs) as output
	DDRA |= 0xFF;;

	// set port B (resistors) as output
	DDRB |= 0xFF;
}

void waitForButtonPress()
{
	os_waitForInput();
	_delay_ms(50);
	os_waitForNoInput();
}

/*!
 * Returns 0 if Uref<Umess and 1 if Uref>Umess; In other means, this function returns PINC0
 */
uint8_t readComparator(void)
{
	return (PINC & (1 << 0)) >> 0;
}

/*
 * Sets Voltage, displays it to LEDs and waits for the current to flow
 */
void setReferenceVoltage(uint8_t refVoltage)
{
	PORTB = refVoltage;
	PORTA = refVoltage;

	_delay_ms(50);
}

/*!
 * Returns an integer indicating, if the parameter lies within the bounds of an 8-Bit integer.
 */
uint8_t referenceVoltageInBounds(uint16_t ref)
{
	return (0 < ref) && (ref < 255);
}



