#include "bin_clock.h"
#include <avr/io.h>
#include <avr/interrupt.h>

//! Global variables
uint8_t milliseconds; /* Since the interrupt is called every 10ms, lets stick with 8 bit and count milliseconds*10 */
uint8_t seconds;
uint8_t minutes;
uint8_t hours;

/*!
 * \return The milliseconds counter of the current time.
 */
uint16_t getTimeMilliseconds()
{
	return ((uint16_t)milliseconds * 10);
}

/*!
 * \return The seconds counter of the current time.
 */
uint8_t getTimeSeconds()
{
	return seconds;
}

/*!
 * \return The minutes counter of the current time.
 */
uint8_t getTimeMinutes()
{
	return minutes;
}

/*!
 * \return The hour counter of the current time.
 */
uint8_t getTimeHours()
{
	return hours;
}


/*
 *	Initializes the binary clock (ISR and global variables)
 */
void initClock(void)
{
	// set timer mode to CTC
	TCCR0A &= ~(1 << WGM00);
	TCCR0A |=  (1 << WGM01);
	TCCR0B &= ~(1 << WGM02);

	// set prescaler to 1024
	TCCR0B |=  (1 << CS02) | (1 << CS00);
	TCCR0B &= ~(1 << CS01);
	
	// set compare register to 195 -> match every 10ms
	OCR0A = 195;

	// init variables
	milliseconds = 0;
	seconds = 50;
	minutes = 59;
	hours = 12;

	// enable timer and global interrupts
	TIMSK0 |= (1 << OCIE0A);
	sei();
}


/*
 *	Updates the global variables to get a valid 12h-time
 */
void updateClock(void)
{
	milliseconds %= 100;
	if(milliseconds == 0) {
		seconds++;
		seconds %= 60;
		if(seconds == 0) {
				minutes++;
				minutes %= 60;
				if(minutes == 0) {
					hours++;
					if(hours == 13) {
						hours = 1;
					}
				}
		}
	}
}


/*
 *	ISR to increase second-counter of the clock
 */
ISR(TIMER0_COMPA_vect)
{
	milliseconds ++;
	updateClock();
}
