#include "lcd.h"

//! Global var, stores character count.
/*!
 *  \internal
 *  This value is in [0;32] 
 * 			... yes, this is no mistake it can be both 0 and 32
 */
unsigned char charCtr;

/*!
 *  Internally used to turn on LCD Port E for 1us.
 *	\internal
 */
extern void lcd_enable(void)
{
	sbi(LCD_PORT_DATA,5); //enable
	_delay_us(1);
	cbi(LCD_PORT_DATA,5); //disable
	return;
}


/*!
 *  Readies the LCD to be used with the defined output-port.
 *	Delay times as specified with some reserve
 */
extern void lcd_init(void)
{
	// write on LCD Port (reading is not needed)
	LCD_PORT_DDR = 0xFF;
	// init routine (see specifications)
	delayMs(15);
	LCD_PORT_DATA = LCD_INIT;
	lcd_enable();
	delayMs(5);
	lcd_enable();
	delayMs(1);
	lcd_enable();
	delayMs(1);
	/////////////////////////////
	// lcd is connected with 4 ports for data
	LCD_PORT_DATA = LCD_4BIT_MODE;
	lcd_enable();
	delayMs(1);
	// display type is 2 line / 5x7 chars
	lcd_command(LCD_TWO_LINES | LCD_5X7);
	lcd_command(LCD_DISPLAY_ON | LCD_HIDE_CURSOR);
	// do not increment DDRAM address or move display
	lcd_command(LCD_NO_INC_ADDR | LCD_NO_MOVE);
	lcd_clear();


	//register custom characters
	lcd_registerCustomChar(LCD_CC_IXI,			LCD_CC_IXI_BITMAP);
	lcd_registerCustomChar(LCD_CC_TILDE,		LCD_CC_TILDE_BITMAP);
	lcd_registerCustomChar(LCD_CC_DEGREE,		LCD_CC_DEGREE_BITMAP);
	lcd_registerCustomChar(LCD_CC_ACCENT,		LCD_CC_ACCENT_BITMAP);
	lcd_registerCustomChar(LCD_CC_BACKSLASH,	LCD_CC_BACKSLASH_BITMAP);
	lcd_registerCustomChar(LCD_CC_MU,			LCD_CC_MU_BITMAP);

	return;
}

/*!
 *  Moves the cursor to the first character of the first line of the LCD.
 */
extern void lcd_line1(void)
{
	lcd_command(LCD_LINE_1);
	charCtr=0;
	return;
}

/*!
 *  Moves the cursor to the first character of the second line of the LCD.
 */
extern void lcd_line2(void)
{
	lcd_command(LCD_LINE_2);
	charCtr=16;
	return;
}

/*!
 * Moves the cursor one step back.
 */
extern void lcd_back(void) {
	lcd_goto(1+(charCtr-1)/16,1+(charCtr-1)%16);
}
/*!
 * Moves the cursor one step forward.
 */
extern void lcd_forward(void) {
	lcd_goto(1+(charCtr+1)/16,1+(charCtr+1)%16);
}

/*!
 * Moves the cursor to the first char
 */
extern void lcd_home(void) {
	lcd_goto(1+charCtr/16,0);
}

/*!
 * Relatively moves the cursor on the LCD.
 */
extern void lcd_move(char row, char column) {
		//there are two rows
	lcd_goto(1+(2+charCtr/16+row)%2, 1+(16+charCtr+column)%16);
}

/*!
 *  Moves the cursor to a specific position on the LCD. Row and column start
 *  counting at 1, so (1,1) is top left, (2,16) is bottom right. 
 *  \param row 		The row to jump to (may be 1 or 2).
 *  \param column 	The column to jump to (may be 1...16).
 */
extern void lcd_goto(unsigned char row, unsigned char column)
{
	// restrict params to valid values
	if(--row>1)
		row=0;
	if(--column>15)
		column=0;
	
	// calculate position and get command
	char command = LCD_CURSOR_MOVE_R + column + row*LCD_NEXT_ROW;

	// update char counter
	charCtr=row*16+column;
	
	lcd_command(command);
	return;
}

/*!
 * Sends a stream to the lcd. The stream is a two-char pair wich either 
 *   holds a command or a printable char. 
 *   This function is used by lcd_command and lcd_writeChar.
 * \param firstByte The first value to send.
 * \param secondByte The second value to send.
 */
void lcd_sendStream(unsigned char firstByte, unsigned char secondByte) 
{
	// check if interrupts are set and store that state
	bool isInterrupt = SREG >> 7;
	// interrupts off
	cli();
    // Simulated LCD does not support the busy flag technique
    if (!SIMULATION)
	{
		bool busy = false;
		do	// wait while LCD is busy
		{
			// read busy flag state:
			// set R/W port to high, all others to low
			LCD_PORT_DATA = 0x40;
			// set enable port to high to read first nibble
			sbi(LCD_PORT_DATA,5);
			// enable reading from pins 1 to 4
			LCD_PORT_DDR = 0xF0;
			// set pull-ups
			LCD_PORT_DATA |= 0x0F;
			// read busy flag (port 4) and store state to 'busy'
			busy = LCD_PIN & 0x08;
			// set enable port back to low
			cbi(LCD_PORT_DATA,5);
			// second nibble is not used, waste it by calling lcd_enable
			lcd_enable();
		}
		while(busy);

		// transmit command:
		LCD_PORT_DDR = 0xFF;
    }

	// send first Byte
	LCD_PORT_DATA = firstByte;
	lcd_enable();
	// send second Byte
	LCD_PORT_DATA = secondByte;
	lcd_enable();

	// restore interrupt flag
	if(isInterrupt)
		sei();

	return;
}

/*!
 *  Sends a specific command to the LCD. This function is only used
 *  internally. There is no need to explicitly call it as is functinality is
 *  capsulated within the other functions.
 *  \param command The command to be executed.
 *	\internal
 */
extern void lcd_command(unsigned char command)
{
	lcd_sendStream((command >> 4) & 0x0F, command & 0x0F);
}

/*!
 *  Writes an 8-Bit ASCII-value to the LCD. 
 *	Features automatic line breaks.
 *  \param character	The character to be written.
 */
extern void lcd_writeChar(char character)
{
	// check if interrupts are set and store that state
 	bool isInterrupt = SREG >> 7;
	cli();

	// check if line shall be changed
	if (character=='\n')
		charCtr = (charCtr & 0x10)+0x10; // <16 -> 16, <32 -> 32
	
	if (charCtr == 0x10)
		lcd_line2();
	if (charCtr == 0x20)
	{
		lcd_clear();
		lcd_line1();
	}

	if(character!='\n')
	{	
		// check for non-ASCII characters the LCD knows
		switch (character) {
			case '�' :	character = 0xE1; break;
			case '�' :	character = 0xEF; break;
			case '�' :	character = 0xF5; break;
			case '�' :	character = 0xE2; break;
			case  8  :	character = LCD_CC_IXI; break;
			case  9  :
			case '~' :	character = LCD_CC_TILDE; break;
			case '\\':	character = LCD_CC_BACKSLASH; break;
			case '�' :	character = LCD_CC_MU; break;
			case '�' :	character = LCD_CC_DEGREE; break;
			case '�' :	character = LCD_CC_ACCENT; break;
		}

		lcd_sendStream(0x10 | ((character & 0xF0) >> 4), 0x10 | (character & 0x0F) );

		// update char counter ... Do not modulo it down! we need it to become 32
		charCtr++;
	}
	
	// restore interrupt flags
	if(isInterrupt)
		sei();

	return;
}

/*!
 *  Erases the LCD and positiones the cursor at the top left corner.
 */
extern void lcd_clear(void)
{
	charCtr = 0;
	lcd_command(LCD_CLEAR);
	return;
}

/*!
 *  Erases one line of the LCD. Cursor will not be changed.
 *  \param line	the line which will be erased (may be 1 or 2).
 */
extern void lcd_erase(unsigned char line)
{
	// save counter
	unsigned char i=0, oldCtr = charCtr;
    // restrict param to a valid value
    if (line > 2)
        line = 1;
	// go to the beginning of line
	lcd_goto(line, 1);
	// clear the line
	for(i=0; i<16; i++)
		lcd_writeChar(' ');
	// restore counter
	charCtr = oldCtr;
	// restore cursor
	lcd_goto((charCtr/16)+1, (charCtr%16)+1);
	return;
}

/*!
 *  Writes a hexadecimal half-byte (one nibble)
 *  \param number	The number to be written.
 *	\param prefix	Writes 0x as prefix if true.
 */
extern void lcd_writeHexNibble(unsigned char number, bool prefix)
{
	if(prefix)
	{
		lcd_writeChar('0');
		lcd_writeChar('x');
	}

	// get low and high nibble
	unsigned char low = number & 0xF;

	if(low<10)
		lcd_writeChar(low+'0');			// write as ASCII number
	else
		lcd_writeChar(low-10 +'A');		// write as ASCII letter

	return;
}

/*!
 *  Writes a hexadecimal byte (equals two chars)
 *  \param number	The number to be written.
 *	\param prefix	Writes 0x as prefix if true.
 */
extern void lcd_writeHex(unsigned char number, bool prefix)
{
	if(prefix)
	{
		lcd_writeChar('0');
		lcd_writeChar('x');
	}

	// get low and high nibble
	unsigned char low = number & 0x0F;
	unsigned char high = number >> 4;
	
	if(high<10)
		lcd_writeChar(high+'0');		    // write as ASCII number
	else			
		lcd_writeChar(high-10 +'A');	    // write as ASCII letter

	if(low<10)
		lcd_writeChar(low+'0');			// write as ASCII number
	else
		lcd_writeChar(low-10 +'A');		// write as ASCII letter

	return;
}

/*!
 *  Writes an int as a number
 */
extern void	lcd_writeDec(uint16_t number)
{
	if (!number)
	{
		lcd_writeChar('0');
		return;
	}
	unsigned int digit = 10000;
    unsigned int nextDigit; //used for optimisation (division is expensive!)
	while (digit>number)
		digit /= 10; //ommit leading zeros
	digit *= 10;
	while (digit > 1)
	{
        nextDigit = digit / 10;
		lcd_writeChar('0'+((number % digit) / nextDigit));
		digit = nextDigit;
	}
    return;
}

/*! \brief Prints the passed voltage onto the display (three float places).
 *
 * \param voltage			Binary voltage value.
 * \param voltUpperBound	Upper bound of the binary voltage value (i.e. 1023 for 10-bit value).
 * \param valueUpperBound	Upper bound of the float voltage value (i.e. 5 for 5V).
 */
extern void lcd_writeVoltage(uint16_t voltage, uint16_t valueUpperBound, uint8_t voltUpperBound)
{
	uint8_t  intVal;
	uint16_t floatVal;

	// Calculate integer and float part of the voltage
	voltage *= voltUpperBound;
	intVal   = voltage / valueUpperBound;
	floatVal = (uint32_t)(voltage - (intVal * valueUpperBound)) * 1000 / valueUpperBound;
		
	// Show voltage on display
	lcd_writeDec(intVal);
	lcd_writeChar('.');
	if (floatVal < 100) lcd_writeChar('0');
	if (floatVal < 10)  lcd_writeChar('0');
	lcd_writeDec(floatVal);
	lcd_writeChar('V');
}



/*!
 *  Writes a string of 8-Bit ASCII-values to the LCD. This function
 *  features automated line breaks. If you reach the end of one
 *  line, the next line will not be erased but the cursor will 
 *	jump to the beginning of the next line. The string needs to be 
 *	terminated correctly.
 *  \param text		The string to be written (a pointer to the first
 *                  character).
 */
extern void lcd_overwriteString(const char* text)
{
	char character;
	while((character = *text++))
	{
		// write char
		lcd_writeChar(character);
	}
	return;
}

/*!
 *  Writes a string of 8-Bit ASCII-values to the LCD. This function
 *  features automated line breaks. If you reach the end of one
 *  line, the next line will be erased and the cursor will 
 *	jump to the beginning of the next line. The string needs to be 
 *	terminated correctly.
 *  \param text		The string to be written (a pointer to the first
 *                  character).
 */
extern void lcd_writeString(const char* text)
{
	char character;
	while((character = *text++))
	{
		// start again at line1
		if(charCtr==16 || charCtr==32)
		{
			lcd_erase((charCtr/16)+1);
		}
		// write char
		lcd_writeChar(character);
	}
	return;
}


/*!
 *  Writes a drawbar
 *	\param percent Percent of drawbar to be drawn
 */
extern void lcd_drawBar(unsigned char percent)
{
	lcd_clear();
	// calculate number of bars
	unsigned int val = percent;
    val *= 16; // 16 Bars are possible on the Display
	unsigned int i=0;
	// draw bars
	for(i=0; i<val; i+=100)
		lcd_writeChar(LCD_CHAR_BAR);
}


/*!
 *  Writes a string of 8-Bit ASCII-values from the program flash memory to
 *  the LCD. This function features automated line breaks as long as you
 *  don't manually do something with the cursor. This works fine in concert
 *  with lcd_write_char without messing up anything.\n
 *  After 32 character have been written, the LCD will be erased and the
 *  cursor starts again at the first character of the first line.
 *  So the function will automaticall seize output after 32 characters.\n
 *  This is a mighty tool for saving SRAM memory.
 *  \param string	The string to be written (a pointer to the first
 *					character).
 */

extern void lcd_writeProgString(const prog_char* string)
{
	unsigned char i = 0;
	for (i = 0; i < 32; i++)
	{
		char c = (char) pgm_read_byte (string);
		if ( c != '\0' )
		{
			lcd_writeChar(c);
			delayMs(1);
		}
		else
		{
			i = 32;
		}
		string++; 
	}
}


/*!
 * Registers a custom char with the LCD CGRAM.
 *   \param addr is the adsress where the character is to be stored. This value is interpreted modulo 8.
 *   \param chr The passed value is one 32 bit integer witch holds all rows of the character.
 */
void lcd_registerCustomChar(unsigned char addr, unsigned long long chr) {
	unsigned char intB = SREG & (1<<7);
	cli();
	lcd_command(0b01000000 | (0b00111000 & (addr << 3)));
	_delay_us(40);

	unsigned char i = 8;
	while (i--) {
		unsigned char row = chr & 0xFF;
		lcd_sendStream(((1<< LCD_RS_PIN) & 0xF0) | ((row >> 4) & 0xF), ((1<< LCD_RS_PIN) & 0xF0) | (row & 0xF));
		_delay_us(40);
		chr >>= 8;
	}
	SREG |= intB;
}

