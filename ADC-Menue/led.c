#include "led.h"
#include <avr/io.h>

uint16_t activateLedMask = 0xFFFF;


/*
 *	Initializes the led bar. Note: All PORTs will be set to output.
 */
void initLedBar(void)
{
	// Set PORTA and PORTD as output
	DDRA |= 0xFF & activateLedMask;
	DDRD |= 0xFF & (activateLedMask >> 8);

	// Turn off LEDs
	PORTA = 0xFF & activateLedMask;
	PORTD = 0xFF & (activateLedMask >> 8);
}


/*
 *	Sets the passed value as states of the led bar (1 = on, 0 = off).
 */
void setLedBar(uint16_t value)
{
	// Apply LED mask
	value &= activateLedMask;

	// Extract LED states for D and A
	uint8_t ledStateD = ((value & 0xFF00) >> 8);
	uint8_t ledStateA = (value & 0x00FF);
	
	// Set inverted states, since the board uses inverse logic
	PORTD = ~ledStateD;
	PORTA = ~ledStateA;
}
