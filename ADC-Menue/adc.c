#include "adc.h"
#include <avr/io.h>
#include <stdlib.h>
#include <util/delay.h>
#include "lcd.h"

//! Global variables
uint16_t  lastCaptured;

uint16_t* bufferStart;
uint8_t   bufferSize;
uint8_t   bufferIndex;

#define CAPTURE_COUNT 16

/*! \brief This method initializes the necessary registers for using the ADC module. \n
 * Reference voltage:    internal \n
 * Input channel:        PORTA0 \n
 * Conversion frequency: 156kHz
 */
void initAdc(void)
{
	// init DDRA0 as input
	DDRA &= ~(1 << PA0);

	// REFS1:0 = 01     select internal reference voltage
	// ADLAR   = 0      store the result right adjusted to the ADC register
	// MUX4:0  = 00000  use ADC0 as input channel (PA0)		
	ADMUX = (0 << REFS1) | (1 << REFS0);

	// ADEN    = 1      enable ADC
	// ADSC    = 0      used to start a conversion
	// ADATE   = 0      no continous conversion
	// ADIF    = 0      indicates that the conversion has finished
	// ADIE    = 0      do not use interrupts
	// ADPS2:0 = 111    prescaler 128 -> 20MHz / 128 = 156kHz ADC frequency
	ADCSRA = (1 << ADEN) | (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);
}


/*! \brief Executes one conversion of the adc and returns its value.
 *
 * \return The converted voltage (0 = 0V, 1023 = AVCC)
 */
uint16_t getAdcValue()
{
	uint16_t allCaptures = 0;
	 
	for(int i = 0; i < CAPTURE_COUNT; i++) {

		// Start the conversion
		ADCSRA |= (1 << ADSC);

		// Wait until the conversion has finished
		while (ADCSRA & (1 << ADSC)) {};

		// add value to the other caputures
		uint16_t currentCapture  = ADCL | ((uint16_t)ADCH << 8);
		allCaptures += currentCapture;
	}

	// Store the value as last captured
	lastCaptured = allCaptures / CAPTURE_COUNT;

	// Return the result;
	return lastCaptured;
}


/*! \brief Returns the size of the buffer which stores voltage values.
 *
 * \return The size of the buffer which stores voltage values.
 */
uint16_t getBufferSize()
{
	return bufferSize;
}


/*! \brief Returns the current index of the buffer which stores voltage values.
 *
 * \return The current index of the buffer which stores voltage values.
 */
uint16_t getBufferIndex()
{
	return bufferIndex;
}


/*! \brief Stores the last captured voltage.
 *
 */
void storeVoltage(void)
{
	// Lazy initialization of buffer
	if(bufferSize == 0) {
		bufferSize = 100;
		bufferStart = malloc(bufferSize * sizeof(uint16_t));

		// If we could not allocate memory, print error message, reset bufferSize and do nothing.
		if(bufferStart == 0) {
			lcd_writeProgString(PSTR("!!! ERROR !!!"));
			lcd_line2();
			lcd_writeProgString(PSTR("MEMORY ERROR"));
			bufferSize = 0;
			_delay_ms(1000);
			return;
		}

		bufferIndex = 0;
	}

	// Do nothing if we're running out of buffer space
	if(bufferIndex >= getBufferSize()) return;
	
	*(bufferStart + bufferIndex) = lastCaptured;
	bufferIndex++;
}


/*! \brief Returns the voltage value with the passed index.
 *
 * \param ind	Index of the voltage value.
 * \return 		The voltage value with index ind.
 */
uint16_t getStoredVoltage(uint8_t ind)
{
	if(0 <= ind && ind < bufferSize) return *(bufferStart + ind);
	return 0;
}
