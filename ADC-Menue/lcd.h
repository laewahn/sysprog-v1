/*! \file
 *  \brief Library for LCD use.
 *
 *  Contains all the essential funtionalities to comfortably work with the
 *  LCD on the evaluation board.
 *
 *  \author		Lehrstuhl Informatik 11 - RWTH Aachen
 *  \date		2012
 *  \version	2.1
 *  \ingroup	lcd_group
 */

#ifndef _LCD_LCD_H
#define _LCD_LCD_H

#define sbi(ADDRESS,BIT) (ADDRESS |= (1<<BIT))
#define cbi(ADDRESS,BIT) (ADDRESS &= ~(1<<BIT))

//! Clock fequency for WinAVR delay function
#define F_CPU					20000000UL

#define delayMs(TIME) _delay_ms(TIME)

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdbool.h>
#include <avr/pgmspace.h>
#include "util/delay.h"

// Begin of file -------------------------------------------------------------

//----------------------------------------------------------------------------
// LCD times (depending on the simulation-level)
//----------------------------------------------------------------------------
#ifndef SIMULATION
#define SIMULATION 0
#endif

#if SIMULATION == 0
#define LCD_INIT_TIME 5000
#else
#define LCD_INIT_TIME 100
#endif 

#if SIMULATION
// Set this if simulator is used
#define I_AM_AT_HOME 
#endif

//----------------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------------

//! DDR of Port connected to LCD
#define LCD_PORT_DDR DDRB

//! PORT connected to LCD
#define LCD_PORT_DATA PORTB

//! PIN connected to LCD
#define LCD_PIN PINB

//! First value for init
#define LCD_INIT		  0x03

//! Init mode
#define LCD_4BIT_MODE	  0x02

//! Command to set one line display
#define LCD_ONE_LINE	  0x20

//! Command to set one line display
#define LCD_TWO_LINES	  0x28

//! Command to set 5x7 display
#define LCD_5X7			  0x20

//! Command to set 5x10 display
#define LCD_5X10		  0x24

//! Command to turn on LCD
#define LCD_DISPLAY_ON 	  0x0C

//! Command to turn off LCD
#define LCD_DISPLAY_OFF	  0x08

//! Command to clear LCD
#define LCD_CLEAR		  0x01

//! Command to disable address increment
#define	LCD_NO_INC_ADDR   0x04

//! Command to enable address increment
#define LCD_INC_ADDR	  0x06

//! Command to disable moving display context
#define LCD_NO_MOVE	  	  0x04

//! Command to enable moving display context
#define LCD_MOVE		  0x05

//! Command to jump to first line
#define LCD_LINE_1		  0x80

//! Command to jump to second line
#define LCD_LINE_2		  0xC0

//!
#define LCD_SHOW_CURSOR	  0x0B

#define LCD_HIDE_CURSOR	  0x08

//! Command to move the cursor to its start position
#define LCD_CURSOR_START  0x02

//! Used to calculate cursor position command for moving right
#define LCD_CURSOR_MOVE_R 0x80

//! Used to calculate cursor position command for moving left
#define LCD_CURSOR_MOVE_L 0x00

//! Used to calculate cursor position command for next row
#define LCD_NEXT_ROW	  0x40

//! Character that looks like filled rectangle
#define LCD_CHAR_BAR	  0xFF

#define LCD_RS_PIN		  4

#define LCD_EN_PIN		  5

//----------------------------------------------------------------------------
// Macros
//----------------------------------------------------------------------------

//! Alias for compability to old function lcd_dataByte
#define	lcd_dataByte lcd_putByte

//! Alias for compability to old function lcd_dataString
#define	lcd_dataString lcd_writeString

//! Alias for compability
#define lcd_writeUint lcd_writeNumber

//! Alias for compability
#define lcd_writeIntNumber lcd_writeNumber

//! Macro for compability to old function lcd_writeHexByte
#define lcd_writeHexByte(x) lcd_writeHex(x, false)

//! Macro for compability to old function lcd_writeHexWord
#define lcd_writeHexWord(x) do {lcd_writeHex(((x) >> 8) & 0xFF, false); lcd_writeHex((x) & 0xFF, false);} while(0)

//! Macro for compability. Swaps the lower and upper half-bytes in a byte.
#define swap(x) ((((x) & 0x0F) << 4) | (((x) & 0xF0) >> 4))

//! Changes into the next line. Technically it changes just to the other line.
#define lcd_nextLine() lcd_writeChar('\n')

//! Defines a custom char out of eight rows passed as integer values
#define CUSTOM_CHAR(cc0,cc1,cc2,cc3,cc4,cc5,cc6,cc7) 0 \
      | ((unsigned long long)cc0 << 0*8) \
      | ((unsigned long long)cc1 << 1*8) \
      | ((unsigned long long)cc2 << 2*8) \
      | ((unsigned long long)cc3 << 3*8) \
      | ((unsigned long long)cc4 << 4*8) \
      | ((unsigned long long)cc5 << 5*8) \
      | ((unsigned long long)cc6 << 6*8) \
      | ((unsigned long long)cc7 << 7*8)

#include "lcd_customchars.h"

//----------------------------------------------------------------------------
// Function headers
//----------------------------------------------------------------------------

//! Enable LCD
extern void	lcd_enable		(void);

//! Initialize LCD
extern void	lcd_init		(void);

//! Next output will be written to line 1
extern void	lcd_line1		(void);

//! Next output will be written to line 2
extern void	lcd_line2		(void);

//! Next output will be written at (row, column)
extern void lcd_goto		(unsigned char row, unsigned char column);

//! Next output will be written at (currRow+row, currCol+column)
extern void lcd_move		(char row, char column);

//! Go back one character (does not remove the character)
extern void lcd_back		(void);

//! Go to the first character in line
extern void lcd_home		(void);

//! Go one character right (does not remove the character)
extern void lcd_forward		(void);

//! Send command to LCD
extern void	lcd_command		(unsigned char command);

//! Clear all data from display
extern void	lcd_clear		(void);

//! Erases one line
extern void lcd_erase		(unsigned char line);

//! Write one character
extern void lcd_writeChar	(char character);

//! Write one hexadecimal byte
extern void	lcd_writeHex	(unsigned char character, bool prefix);

//! Write a half-byte (a nibble)
extern void lcd_writeHexNibble(unsigned char number, bool prefix);

//! Write a byte as a number
extern void	lcd_writeDec	(uint16_t number);

//! Write a voltage with valueUpperBound as float voltage with voltUpperBound
extern void lcd_writeVoltage(uint16_t voltage, uint16_t valueUpperBound, uint8_t voltUpperBound);

//! Write text string
extern void lcd_writeString	(const char* text);

//! Write prog_char* string
extern void lcd_writeProgString(const prog_char* string);

//! Write a drawbar
extern void lcd_drawBar		(unsigned char percent);

//! Register a customly designed character with the LCD.
extern void lcd_registerCustomChar(unsigned char addr, unsigned long long chr);


// End of file ---------------------------------------------------------------

#endif

