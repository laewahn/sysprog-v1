#define LCD_CC_IXI 0
#define LCD_CC_IXI_BITMAP (CUSTOM_CHAR( \
      0b00000, \
      0b00000, \
      0b00001, \
      0b00000, \
      0b10101, \
      0b01001, \
      0b10101, \
      0b00000))
// note: 9===1
#define LCD_CC_TILDE 1
#define LCD_CC_TILDE_BITMAP (CUSTOM_CHAR( \
      0b00000, \
      0b01000, \
      0b10101, \
      0b00010, \
      0b00000, \
      0b00000, \
      0b00000, \
      0b00000))
	  

#define LCD_CC_DEGREE 2
#define LCD_CC_DEGREE_BITMAP (CUSTOM_CHAR( \
      0b00100, \
      0b01010, \
      0b00100, \
      0b00000, \
      0b00000, \
      0b00000, \
      0b00000, \
      0b00000))

#define LCD_CC_ACCENT 3
#define LCD_CC_ACCENT_BITMAP (CUSTOM_CHAR( \
      0b00010, \
      0b00100, \
      0b01000, \
      0b00000, \
      0b00000, \
      0b00000, \
      0b00000, \
      0b00000))


#define LCD_CC_BACKSLASH 4
#define LCD_CC_BACKSLASH_BITMAP (CUSTOM_CHAR( \
      0b00000, \
      0b10000, \
      0b01000, \
      0b00100, \
      0b00010, \
      0b00001, \
      0b00000, \
      0b00000))

#define LCD_CC_MU 5
#define LCD_CC_MU_BITMAP (CUSTOM_CHAR( \
      0b00000, \
      0b00000, \
      0b01001, \
      0b01001, \
      0b01001, \
      0b01110, \
      0b01000, \
      0b00000))

