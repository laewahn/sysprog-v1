/*! \file
 *  \brief Analog digital converter
 *
 *  \author   Lehrstuhl f�r Informatik 11 - RWTH Aachen
 *  \date     2012
 *  \version  1.0
 */

#include "lcd.h"
#include "os_input.h"
#include "menu.h"


int main(void)
{
	// 1. initialize the buttons
    os_initInput();

	// 2. initialize lcd
	lcd_init();

	// 3. show menue
	showMenu();
}
