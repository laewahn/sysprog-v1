#include "menu.h"
#include "os_input.h"
#include "bin_clock.h"
#include "lcd.h"
#include "led.h"
#include "adc.h"
#include <stdint.h>
#include <avr/io.h>

#define ESCAPE_MASK (1 << 3)
/*!
 *	Hello world program.
 *	Shows the string 'Hello World!' on the display.
 */
void helloWorld(void)
{
	uint8_t helloWorldState = 0;
	
	// Repeat until ESC gets pressed
	while((os_getInput() & ESCAPE_MASK) == 0) {
		if(helloWorldState == 0) {
			lcd_writeProgString(PSTR("Hello world"));
			helloWorldState = 1;
		} else {
			lcd_clear();
			helloWorldState = 0;
		}

		_delay_ms(500);
	}

	os_waitForNoInput();	
}


/*!
 *	Shows the clock on the display and a binary clock on the led bar.
 */
void displayClock(void)
{	
	lcd_clear();
	while((os_getInput() & ESCAPE_MASK) == 0) {
		
		// Write time to LED bar
		uint16_t clockVal = 0;
		clockVal |= (getTimeSeconds() & 0x3F);
		clockVal |= ((getTimeMinutes() & 0x3F) << 6);
		clockVal |= (getTimeHours() << 12);

		setLedBar(clockVal);

		// Write time to LCD
		lcd_line1();

		uint8_t hours = getTimeHours();
		if(hours < 10) lcd_writeDec(0);
		lcd_writeDec(hours);
		lcd_writeChar(':');

		uint8_t minutes = getTimeMinutes();
		if(minutes < 10) lcd_writeDec(0);
		lcd_writeDec(minutes);
		lcd_writeChar(':');

		uint8_t seconds = getTimeSeconds();
		if(seconds < 10) lcd_writeDec(0);
		lcd_writeDec(seconds);
		lcd_writeChar(':');

		uint16_t milliseconds = getTimeMilliseconds();
		if(milliseconds <= 99) lcd_writeDec(0);
		if(milliseconds <= 9)  lcd_writeDec(0);
		lcd_writeDec(milliseconds);
	}
	
	setLedBar(0x00);
	os_waitForNoInput();
}


/*!
 *	Shows the stored voltage values in the 2nd line of the display.
 */
void displayVoltageBuffer(uint8_t displayIndex)
{
	lcd_goto(2,11);
	lcd_writeVoltage(getStoredVoltage(displayIndex), 1023, 5);
}


/*!
 *	Shows the adc value on the display and on the led bar.
 */
void displayAdc(void)
{
	uint8_t voltageBufferIndex = 1;

	lcd_clear();
	lcd_writeProgString(PSTR("Voltage:  "));
	while((os_getInput() & ESCAPE_MASK) == 0) {
		uint16_t voltage = getAdcValue();
		_delay_ms(100);
		
		// Display voltage on LCD
		lcd_goto(1,11);
		lcd_writeVoltage(voltage, 1023, 5);

		// Display voltage on LED bar
		uint16_t ledValue = 0;
		uint16_t adcResult = voltage;
		
		while(adcResult >= 68) {
			ledValue = (ledValue << 1);
			ledValue |= 2;
			adcResult -= 68;
		}

		setLedBar(ledValue);

		// Enable user interaction for browsing through voltage buffer
		uint8_t pressedButton = os_getInput();
		if (pressedButton == 0b00000001) {	// enter
			storeVoltage();
		}
		if (pressedButton == 0b00000010) { // down
			if(voltageBufferIndex == 1) {
				voltageBufferIndex = 100;
			} else {
				voltageBufferIndex--;
			}
		}
		if (pressedButton == 0b00000100) { // up
			if(voltageBufferIndex == 100) {
				voltageBufferIndex = 1;
			} else {
				voltageBufferIndex++;
			}
		}
		
		// Write current voltage index and stored value to the LCD
		lcd_line2();
		if(voltageBufferIndex <= 99) lcd_writeDec(0);
		if(voltageBufferIndex <= 9)  lcd_writeDec(0);
		lcd_writeDec(voltageBufferIndex);
		lcd_writeProgString(PSTR("/100"));
		displayVoltageBuffer(voltageBufferIndex - 1);
	}
}


/*! \brief Starts the passed program
 *
 * \param programIndex Index of the program to start.
 */
void start(uint8_t programIndex)
{
	// initialize and start the passed 'program'
	switch (programIndex)
	{
	case 0:
		lcd_clear();
		helloWorld();
		break;
	case 1:
		activateLedMask = 0xFFFF;	// use all leds
		initLedBar();
		initClock();
		displayClock();
		break;
	case 2:
		activateLedMask = 0xFFFE;	// don't use led 0
		initLedBar();
		initAdc();
		displayAdc();
		break;
	default:
		break;
	}

	// do not resume to the menu until all buttons are released
	os_waitForNoInput();
}


/*!
 *	Shows a user menu on the display which allows to start subprograms.
 */
void showMenu(void)
{
	uint8_t pageIndex = 0;

	while (1)
	{
		lcd_clear();
		lcd_writeProgString(PSTR("Select:"));	
		lcd_line2();

		switch (pageIndex)
		{
		case 0:
			lcd_writeProgString(PSTR("1: Hello world"));			
			break;
		case 1:
			lcd_writeProgString(PSTR("2: Binary clock"));
			break;
		case 2:
			lcd_writeProgString(PSTR("3: Internal ADC"));
			break;
		default:
			lcd_writeProgString(PSTR("----------------"));
			break;
		}

		os_waitForInput();
		if (os_getInput() == 0b00000001)		// Enter
		{
			os_waitForNoInput();
			start(pageIndex);
		}
		else if (os_getInput() == 0b00000100)	// Up
		{
			os_waitForNoInput();
			pageIndex = (pageIndex + 1) % 3;
		}
		else if (os_getInput() == 0b00000010)	// Down
		{
			os_waitForNoInput();
			if (pageIndex == 0) 
			{
				pageIndex = 2; 
			}
			else 
			{
				pageIndex--;
			}
		}
	}
}
