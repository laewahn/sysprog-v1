/*! \file
 *  \brief Constants for ATMega644 and custom evaluation board.
 *
 *  Defines several useful constants for the ATMega644 and our AVR evaluation
 *  board.
 *
 *  \author		Lehrstuhl f�r Informatik 11 - RWTH Aachen
 *  \date		2012
 *  \version	1.0
 */

#ifndef _ATMEGA644CONSTANTS_H
#define _ATMEGA644CONSTANTS_H

//! Clock fequency on evaluation board in HZ (6 MHZ)
#define AVR_CLOCK_FREQUENCY		20000000

//! Clock fequency for WinAVR delay function
#define F_CPU					20000000UL


#endif
