#include "os_input.h"

#include <avr/io.h>
#include <stdint.h>

/*! \file

Everything that is necessary to get the input from the Buttons in a clean format.

*/

// Convenience defines for button states
#define BTN_UP 0
#define BTN_DOWN 1

// Defines the pins that are used as buttons
#define BTN_MASK 0b00000010


// Helper methods
void initBtnPin(uint8_t);

/*!
 *  A simple "Getter"-Function for the Buttons on the evaluation board.\n
 * 
 *  \returns The state of the button(s) in the lower bits of the return value.\n
 *  example: 1 Button:  -pushed:   00000001
 *                      -released: 00000000
 *           4 Buttons: 1,3,4 -pushed: 000001101
 * 
 */
uint8_t os_getInput(void) 
{
	uint8_t btnRegister = (PINC ^ BTN_MASK) & BTN_MASK;

	// extract upper (6 and 7) and lower (0 and 1) button bits
	uint8_t upperButtonBits = btnRegister & ((1 << 7) | (1 << 6));
	uint8_t lowerButtonBits = btnRegister & ((1 << 1) | (1 << 0));

	return lowerButtonBits | (upperButtonBits >> 4);
}


/*!
 *	Initializes PIN and PORT for input
 */
void os_initInput(void) {
	initBtnPin(1);
}


/*!
 * Initializes the specified pin of PORTC as input
 */
void initBtnPin(uint8_t pin) {
	DDRC &= ~(1 << pin);
	PORTC |= (1 << pin);
}

/*!
 *  Endless loop as long as at least one button is pressed.
 */
void os_waitForNoInput() 
{
	uint8_t buttonState;

	do {
		buttonState = os_getInput();
	} while (buttonState != BTN_UP);

}

/*!
 *  Endless loop until at least one button is pressed.
 */
void os_waitForInput() 
{
	uint8_t buttonState;

	do {
		buttonState = os_getInput();
	} while (buttonState == BTN_UP);
}
